const GO_TO_SHOW_STEP = state => {
  state.cvuStep = 1;
};

const GO_TO_EDIT_STEP = state => {
  state.cvuStep = 2;
};

const SET_WALLET = (state, wallet) => {
  Object.keys(wallet).map((k) => {
    if(k=='ARS' || k=='USD'){
      state.wallet = wallet[k];
      state.wallet['currency'] = k
    }
  });
};

const UPDATE_ALIAS = (state, alias) => {
  state.wallet.alias = alias;
};

export default {
  GO_TO_SHOW_STEP,
  GO_TO_EDIT_STEP,
  SET_WALLET,
  UPDATE_ALIAS
};
