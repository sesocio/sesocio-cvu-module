import Axios from 'axios';

const goToShowStep = ({ commit }) => {
  commit("GO_TO_SHOW_STEP");
};

const goToEditStep = ({ commit }) => {
  commit("GO_TO_EDIT_STEP");
};

const getWallet = async ({ commit }) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/user_bank_accounts/user_bank_wallet`;

      const response = await Axios.get(url)
      commit('SET_WALLET', response.data)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const saveAlias = async ({ state, commit }, alias) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/bank_accounts/update_cvu_to_user`;
      let data = {
        "alias": alias,
        "cvu": state.wallet.cvu,
        "currency": state.wallet.currency
      }
      const response = await Axios.post(url, data)
      commit('UPDATE_ALIAS', alias)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

export default {
  goToShowStep,
  goToEditStep,
  getWallet,
  saveAlias
};
