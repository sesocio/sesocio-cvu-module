import actions from "./actions";
import mutations from "./mutations";

const state = {
  cvuStep: 1,
  wallet: null
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};